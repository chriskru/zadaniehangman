import { HttpClient } from '@angular/common/http';
import { Component, HostListener, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { GameStateType } from './game-state-type';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    gameStateType = GameStateType; // enum reference
    private readonly MAX_WRONG_INPUT_COUNT = 11;
    private _wrong_input_count: number = 0;
    private _right_input_count: number = 0;


    // "wrong_input_text" contains all the wrong input letters, separated by spaces.
    // It's used only for display purposes.
    private _wrong_input_text: string = "";
    private _wrong_input_letter_set: Set<string> = new Set();
    private _word_to_guess: string = "";
    // "is_letter_guessed" contains a boolean value for every letter within the "word_to_guess"
    // The boolean values decide whether the respective letters have been guessed or not.
    private _is_letter_guessed: boolean[] = [];
    private _game_state: GameStateType = GameStateType.InGame;


    get wrongInputCount() {
        return this._wrong_input_count;
    }


    get wrongInputText() {
        return this._wrong_input_text;
    }


    get wordToGuess() {
        return this._word_to_guess;
    }


    get isLetterGuessed() {
        return this._is_letter_guessed;
    }


    get gameState() {
        return this._game_state;
    }




    constructor(private _http_client: HttpClient) { }




    ngOnInit() {
        this.requestNewWord();
    }




    // Send a random word request to the "Random Words API" (https://github.com/mcnaveen/Random-Words-API).
    // Ignores all words over 10 characters long, due to layout constraints.

    // API errors are only shown in console.
    // This could be improved by showing some kind of notification/toast of the error, to the user.
    private requestNewWord() {
        const self = this;
        this._http_client.get("https://random-words-api.vercel.app/word").pipe(first()).subscribe((response: any) => {


            if (response == null) {
                console.log("API Error");
            } else if (response[0] == null) {
                console.log("API Error");
            } else if (response[0].word?.length > 10) {
                self.requestNewWord();
            } else if (response[0].word as string != null) {
                self.saveNewWord((response[0].word as string).toUpperCase());
            } else {
                console.log("API Error");
            }


        }, (error) => {
            console.log(error);
        });
    }




    private saveNewWord(new_word: string) {
        this._word_to_guess = new_word;
        this._is_letter_guessed = new Array<boolean>(this._word_to_guess.length).fill(false);
        this._game_state = GameStateType.InGame;
    }




    public startNewGame() {
        this._wrong_input_text = "";
        this._wrong_input_letter_set.clear();
        this._wrong_input_count = 0;
        this._right_input_count = 0;
        this.requestNewWord();
    }




    @HostListener("window:keydown", ['$event'])
    onKeyDown(event: KeyboardEvent) {
        // Checks if the input key is a letter.
        if (this._game_state == GameStateType.InGame && event.key.length == 1 && event.key.match(/[a-z]/i)) {
            const event_key = event.key.toUpperCase();


            let is_input_right = this.isInputLetterRight(event_key);
            if (!is_input_right && this._wrong_input_count < this.MAX_WRONG_INPUT_COUNT &&
                !this._wrong_input_letter_set.has(event_key)
            ) {
                this.addWrongInputLetter(event_key);
            }


            if (this._right_input_count == this._word_to_guess.length) {
                this._game_state = GameStateType.Victory;
            } else if (this._wrong_input_count == this.MAX_WRONG_INPUT_COUNT) {
                this._game_state = GameStateType.Defeat;
            }
        }
    }




    private isInputLetterRight(event_key: string): boolean {
        let is_input_right = false;
        for (let i_letter = 0; i_letter < this._word_to_guess.length; ++i_letter) {
            if (this._word_to_guess.charAt(i_letter) == event_key) {
                is_input_right = true;


                if (this._is_letter_guessed[i_letter] != true) {
                    this._is_letter_guessed[i_letter] = true;
                    this._right_input_count += 1;
                }
            }
        }


        return is_input_right;
    }




    private addWrongInputLetter(event_key: string): void {
        this._wrong_input_letter_set.add(event_key);
        this._wrong_input_text += " " + event_key;
        this._wrong_input_count += 1;
    }
}
