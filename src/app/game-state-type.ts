export enum GameStateType {
    InGame, Victory, Defeat
}