import { Component, Input } from '@angular/core';


@Component({
    selector: 'app-gallow',
    templateUrl: './gallow.component.html',
    styleUrls: ['./gallow.component.scss']
})
export class GallowComponent {
    @Input() wrongInputCount: number = 0;
}
