import { Component, Input } from '@angular/core';


@Component({
    selector: 'app-wrong-input-information',
    templateUrl: './wrong-input-information.component.html',
    styleUrls: ['./wrong-input-information.component.scss']
})
export class WrongInputInformationComponent {
    // Contains all the wrong input letters, separated by spaces.
    @Input() wrongInputText: string = "";
}
