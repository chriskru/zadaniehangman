import { ComponentFixture, TestBed } from '@angular/core/testing';
import { WrongInputInformationComponent } from './wrong-input-information.component';


describe('WrongInputInformationComponent', () => {
    let component: WrongInputInformationComponent;
    let fixture: ComponentFixture<WrongInputInformationComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [WrongInputInformationComponent]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(WrongInputInformationComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
