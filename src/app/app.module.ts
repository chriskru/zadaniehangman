import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { GallowComponent } from './gallow/gallow.component';
import { GameFinishOverlayComponent } from './game-finish-overlay/game-finish-overlay.component';
import { GuessedWordPanelComponent } from './guessed-word-panel/guessed-word-panel.component';
import { WrongInputInformationComponent } from './wrong-input-information/wrong-input-information.component';


@NgModule({
    declarations: [
        AppComponent,
        GallowComponent,
        WrongInputInformationComponent,
        GuessedWordPanelComponent,
        GameFinishOverlayComponent
    ],


    imports: [
        HttpClientModule,
        BrowserModule
    ],


    providers: [],


    bootstrap: [AppComponent]
})
export class AppModule { }
