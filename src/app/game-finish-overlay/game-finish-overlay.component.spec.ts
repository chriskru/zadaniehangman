import { ComponentFixture, TestBed } from '@angular/core/testing';
import { GameFinishOverlayComponent } from './game-finish-overlay.component';


describe('GameFinishOverlayComponent', () => {
    let component: GameFinishOverlayComponent;
    let fixture: ComponentFixture<GameFinishOverlayComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [GameFinishOverlayComponent]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(GameFinishOverlayComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
