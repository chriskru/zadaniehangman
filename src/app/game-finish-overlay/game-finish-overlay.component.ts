import { Component, EventEmitter, Input, Output } from '@angular/core';
import { GameStateType } from '../game-state-type';




@Component({
    selector: 'app-game-finish-overlay',
    templateUrl: './game-finish-overlay.component.html',
    styleUrls: ['./game-finish-overlay.component.scss']
})
export class GameFinishOverlayComponent {
    gameStateType = GameStateType; // enum
    @Input() gameState: GameStateType = GameStateType.InGame;
    @Output() newGameEvent: EventEmitter<any> = new EventEmitter();




    public onNewGameButtonClick() {
        this.newGameEvent.emit();
    }
}
