import { Component, Input } from '@angular/core';




@Component({
    selector: 'app-guessed-word-panel',
    templateUrl: './guessed-word-panel.component.html',
    styleUrls: ['./guessed-word-panel.component.scss']
})
export class GuessedWordPanelComponent {
    @Input() isLetterGuessed: boolean[] = [];
    @Input() wordToGuess: string = "";
}
