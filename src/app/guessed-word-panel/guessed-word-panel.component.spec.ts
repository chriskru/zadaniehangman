import { ComponentFixture, TestBed } from '@angular/core/testing';
import { GuessedWordPanelComponent } from './guessed-word-panel.component';


describe('GuessedWordPanelComponent', () => {
    let component: GuessedWordPanelComponent;
    let fixture: ComponentFixture<GuessedWordPanelComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [GuessedWordPanelComponent]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(GuessedWordPanelComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
